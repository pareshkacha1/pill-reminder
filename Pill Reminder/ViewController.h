//
//  ViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 10/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
@interface ViewController : UIViewController<FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance>
@property (weak, nonatomic) IBOutlet FSCalendar *calendarView;
@property (assign, nonatomic) NSInteger      theme;
@property (assign, nonatomic) FSCalendarScrollDirection scrollDirection;
@property (assign, nonatomic) BOOL           lunar;
@property (strong, nonatomic) NSDate         *selectedDate;
@property (assign, nonatomic) NSUInteger     firstWeekday;
@property (strong, nonatomic) NSCalendar *lunarCalendar;
@property (strong, nonatomic) NSArray *lunarChars;
@property (strong, nonatomic) NSArray *datesShouldNotBeSelected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopCons;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;
@end

