//
//  NoteViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 29/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)donePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) NSString * selecteDate;
@end
