//
//  SettingViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingCell.h"
#import "SettingSwitchCell.h"
@interface SettingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary * settingDict;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
