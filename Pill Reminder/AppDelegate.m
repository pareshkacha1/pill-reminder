//
//  AppDelegate.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 10/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.isAppStarted = YES;
    application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    setSnoozeClosedDate([NSDate date]);
    if(!application.isRegisteredForRemoteNotifications)
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    if(getFirstTimeLaunch == 0)
    {
        setFirstTimeLaunch(1);
        setcontraception(@"Pills");
        setreminderDays(@"21/7,21 days + 7 days off");
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                       fromDate:[NSDate date]];
        [timeComponents setHour:0];
        [timeComponents setMinute:00];
        [timeComponents setSecond:0];
        
        NSDate *dtFinal = [calendar dateFromComponents:timeComponents];
        setstartDate(dtFinal);
        
        setreminder(NO);
        
        [timeComponents setHour:9];
        [timeComponents setMinute:00];
        [timeComponents setSecond:0];
        
        dtFinal = [calendar dateFromComponents:timeComponents];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm Z"];   
        setreminderTime(dtFinal);
        setmessageForPill(@"Take a pill");
        setmessageForPlacebo(@"Take a placebo pill");
        setmessageForPatchApply(@"Apply new patch");
        setmessageForPatchRemove(@"Remove patch");
        setmessageForRingInsert(@"Insert new ring");
        setmessageForRingRemove(@"Remove Ring");
        setrefillReminder(NO);
        setrefillReminderDaysAlarm(@"3 Days Ahead");
        setrefillReminderTime(dtFinal);
        setrefillMessage(@"Get Contraception");
        setWeekStart(@"Monday");
        setSounds(@"Default");
        setSoundsRefill(@"Default");
        setPreHours(0);
        setPreMins(0);
        self.Days = 0;
        self.PlaceBo = 0;
        self.BlankDays = 0;
        [[NSUserDefaults standardUserDefaults]setValue:@"No Auto-Snooze" forKey:@"Auto-Snooze"];
    }
    [self getDaysStructure];
    return YES;
}
- (void)getDaysStructure
{
    if([getcontraception isEqualToString:@"Pills"])
    {
        switch ([@[@"21/7,21 days + 7 days off",@"21+7,21 days + 7 days of placebo pills",@"24/4,24 days + 4 days off",@"24+4,24 days +4 days of placebo pills",@"28,28 days",@"35,35 days",@"84+7,84 days  + 7 days of placebo pills",@"Custom"] indexOfObject:getreminderDays]) {
            case 0:
                self.Days = 21;
                self.PlaceBo = 0;
                self.BlankDays = 7;
                break;
            case 1:
                self.Days = 21;
                self.PlaceBo = 7;
                self.BlankDays = 0;
                break;
            case 2:
                self.Days = 24;
                self.BlankDays = 4;
                self.PlaceBo = 0;
                break;
            case 3:
                self.Days = 24;
                self.PlaceBo = 4;
                self.BlankDays = 0;
                break;
            case 4:
                self.Days = 28;
                self.PlaceBo = 0;
                self.BlankDays = 0;
                break;
            case 5:
                self.Days = 35;
                self.BlankDays = 0;
                self.PlaceBo = 0;
                break;
            case 6:
                self.Days = 84;
                self.PlaceBo = 7;
                self.BlankDays = 0;
                break;
            default:
                self.Days = getDaysCount;
                self.PlaceBo = getPlaceboCount;
                self.BlankDays = getBlankCounts;
                break;
        }
        setDaysCount(self.Days);
        setPlaceboCount(self.PlaceBo);
        setBlankCounts(self.BlankDays);
        if(self.BlankDays == 0)
        {
            setBlankDays(NO);
        }
        else
        {
            setBlankDays(YES);
        }
    }
    else if([getcontraception isEqualToString:@"Ring"])
    {
        self.Days = 21;
        self.PlaceBo = 0;
        self.BlankDays = 7;
    }
    else
    {
        self.Days = 21;
        self.PlaceBo = 0;
        self.BlankDays = 7;
    }
}
- (void)refillReminder:(NSDate *)date
{
    if(getrefillReminder)
    {
        NSCalendar *calendarStartDate = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponentsStartDate = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                                         fromDate:date];
        
        NSDateComponents *timeComponents = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                                fromDate:getrefillReminderTime];
        int dayBefore = [getrefillReminderDaysAlarm stringByReplacingOccurrencesOfString:@" Days Ahead" withString:@""].intValue;
        [timeComponents setDay:timeComponentsStartDate.day - dayBefore];
        [timeComponents setMonth:timeComponentsStartDate.month];
        [timeComponents setYear:timeComponentsStartDate.year];
        [timeComponents setSecond:0];
        
        NSDate *dtFinal = [calendarStartDate dateFromComponents:timeComponents];
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = dtFinal;
        notification.alertBody = getrefillMessage;
        notification.soundName = UILocalNotificationDefaultSoundName;
        notification.applicationIconBadgeNumber = 5;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}
- (void)pillReminder:(NSDate *)date notiMessage:(NSString *)notiMessage
{
    NSCalendar *calendarStartDate = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *timeComponentsStartDate = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                                     fromDate:date];
    
    NSDateComponents *timeComponents = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                            fromDate:getreminderTime];
    [timeComponents setDay:timeComponentsStartDate.day];
    [timeComponents setMonth:timeComponentsStartDate.month];
    [timeComponents setYear:timeComponentsStartDate.year];
    [timeComponents setSecond:0];
    NSDate *dtFinal = [calendarStartDate dateFromComponents:timeComponents];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Auto-Snooze"] isEqualToString:@"No Auto-Snooze"])
    {
        if(getreminder)
        {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = dtFinal;
            notification.alertBody = notiMessage;
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 4;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Auto-Snooze"] isEqualToString:@"Repeat every hour"] && getIsPaid)
    {
        if(getreminder)
        {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = dtFinal;
            notification.alertBody = notiMessage;
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 3;
            if( [dtFinal timeIntervalSinceDate:[NSDate date]] > 0 )
            {
                notification.repeatInterval = NSCalendarUnitHour;
            }
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Auto-Snooze"] isEqualToString:@"Repeat every minute"] && getIsPaid)
    {
        if(getreminder)
        {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = dtFinal;
            notification.alertBody = notiMessage;
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 2;
            if( [dtFinal timeIntervalSinceDate:[NSDate date]] > 0 )
            {
                notification.repeatInterval = NSCalendarUnitMinute;
            }
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
    else
    {
        if(getIsPaid && getreminder)
        {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = dtFinal;
            notification.alertBody = notiMessage;
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 4;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            if( [dtFinal timeIntervalSinceDate:[NSDate date]] > 0 )
            {
                for (int i = 1; i <= getSnoozeTimes; i++)
                {
                    UILocalNotification *notification = [[UILocalNotification alloc] init];
                    notification.alertBody = notiMessage;
                    notification.soundName =  UILocalNotificationDefaultSoundName;
                    notification.applicationIconBadgeNumber = 1;
                    notification.fireDate = [dtFinal dateByAddingTimeInterval:getSnoozeMinutes * i * 60];
                    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
                }
            }
        }
    }
    if((getPreHours > 0 || getPreMins > 0) && getIsPaid)
    {
        if(getreminder)
        {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.alertBody = notiMessage;
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber = 6;
            notification.fireDate = [dtFinal dateByAddingTimeInterval:-1 * (getPreMins * 60) - 1 * (getPreHours * 60 * 60)];
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
}
-(void)refillReminderNotification
{
    for (NSString * dateStr in self.startDates)
    {
        NSDate * date = [_calendarView dateFromString:dateStr format:@"yyyy-MM-dd"];
        if(![date isEqualToDate:getstartDate])
        {
            [self refillReminder:date];
        }
    }
}
-(void)pillReminderNotification
{
    NSString * notiMessage;
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                   fromDate:[NSDate date]];
    [timeComponents setHour:0];
    [timeComponents setMinute:00];
    [timeComponents setSecond:0];
    
    NSDate *dtFinal = [calendar dateFromComponents:timeComponents];

    for (NSString * dateStr in self.startDates)
    {
        NSDate * date = [_calendarView dateFromString:dateStr format:@"yyyy-MM-dd"];
        
        if( [date timeIntervalSinceDate:dtFinal] >= 0 ) {
            if([getcontraception isEqualToString:@"Pills"])
            {
                notiMessage = getmessageForPill;
            }
            else if ([getcontraception isEqualToString:@"Ring"])
            {
                notiMessage = getmessageForRingInsert;
            }
            else
            {
                notiMessage = getmessageForPatchApply;
            }

            [self pillReminder:date notiMessage:notiMessage];
        }
    }
    for (NSString * dateStr in self.datesWithEvent)
    {
        NSDate * date = [_calendarView dateFromString:dateStr format:@"yyyy-MM-dd"];
        
        if( [date timeIntervalSinceDate:dtFinal] >= 0 )
        {
            if([getcontraception isEqualToString:@"Pills"])
            {
                notiMessage = getmessageForPill;
                [self pillReminder:date notiMessage:notiMessage];
            }
            else if ([getcontraception isEqualToString:@"Ring"])
            {
                notiMessage = getmessageForRingInsert;
//                [self pillReminder:date notiMessage:notiMessage];
            }
            else
            {
                NSCalendar *gregorian = [NSCalendar autoupdatingCurrentCalendar];
                NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:getstartDate];
                NSInteger weekday = [comps weekday];
                
                NSDateComponents *compsForCurrentDate = [gregorian components:NSCalendarUnitWeekday fromDate:date];
                NSInteger weekdayForCurrentDate = [compsForCurrentDate weekday];

                if(weekday == weekdayForCurrentDate)
                {
                    notiMessage = getmessageForPatchApply;
                    [self pillReminder:date notiMessage:notiMessage];
                }
            }
        }
    }
    for (NSString * dateStr in self.datesWithEventForPlacebo)
    {
        NSDate * date = [_calendarView dateFromString:dateStr format:@"yyyy-MM-dd"];

        if( [date timeIntervalSinceDate:dtFinal] >= 0 )
        {
            if([getcontraception isEqualToString:@"Pills"])
            {
                notiMessage = getmessageForPlacebo;
                [self pillReminder:date notiMessage:notiMessage];
            }
        }
    }
    
}
- (void)PrepareReminder
{
    self.datesWithEvent = [NSMutableArray new];
    self.datesWithEventForPlacebo = [NSMutableArray new];
    self.startDates = [NSMutableArray new];
    int count = 0;
    for (NSDate *date = getstartDate; [date compare: [_calendarView maximumDate]] < 0; date = [date dateByAddingTimeInterval:24 * 60 * 60*1] )
    {
        if(count == 0)
        {
            [self.startDates addObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"] ];
        }
        else if(count < self.Days)
        {
            [self.datesWithEvent addObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"] ];
        }
        else
        {
            if(![getcontraception isEqualToString:@"Pills"])
            {
                NSDate * removeDate = [_calendarView dateByAddingDays:21 toDate:getstartDate];
                if([date isEqualToDate:removeDate])
                {
                    NSString * notiMessage;
                    if(![getcontraception isEqualToString:@"Ring"])
                    {
                        notiMessage = getmessageForRingRemove;
                    }
                    else
                    {
                        notiMessage = getmessageForPatchRemove;
                    }
                    [self pillReminder:date notiMessage:notiMessage];
                }
            }
            if(self.PlaceBo != 0)
            {
                [self.datesWithEventForPlacebo addObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"] ];
            }
        }
        count++;
        if(count == self.Days+self.BlankDays+self.PlaceBo)
        {
            count = 0;
        }
    }
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [_calendarView reloadData];
    [self pillReminderNotification];
    [self refillReminderNotification];
}
- (void)weekStartDay
{
    NSString * day = getWeekStart;
    if([day isEqualToString:@"Monday"])
    {
        _calendarView.firstWeekday = 2;
    }
    else if([day isEqualToString:@"Saturday"])
    {
        _calendarView.firstWeekday = 7;
    }
    else
    {
        _calendarView.firstWeekday = 1;
    }
}
-(void)updateCalendarAndNotification
{
    [self getDaysStructure];
    [self PrepareReminder];
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
@end
