//
//  PickerViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 26/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "PickerViewController.h"
#import "AppDelegate.h"
@implementation PickerViewController
@synthesize identifier;
-(void)viewDidLoad
{
    if([identifier isEqualToString:@"Start Date"])
    {
        _topViewHeightCons.constant = 0;
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        [self.datePicker addTarget:self action:@selector(datePcikerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.pickerView setHidden:YES];
        [self.datePicker setHidden:NO];
        [self.datePicker setDate:getstartDate];
        if([getcontraception isEqualToString:@"Pills"])
        {
            _BottomLabel.text = @"Set the date of the first pill in your current or last pack";
        }
        else if ([getcontraception isEqualToString:@"Ring"])
        {
            _BottomLabel.text = @"Set the date of when you inserted the ring";
        }
        else
        {
            _BottomLabel.text = @"Set the date of when you started the current cycle of petches";
        }
        self.title = @"Start Date";
        preservedDate = getstartDate;
    }
    else if([identifier isEqualToString:@"Reminder Time"])
    {
        _topViewHeightCons.constant = 0;
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];
        [self.datePicker addTarget:self action:@selector(datePcikerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.pickerView setHidden:YES];
        [self.datePicker setHidden:NO];
        [self.datePicker setDate:getreminderTime];
        _BottomLabel.text = @"Set the time when you want to be reminded";
        self.title = @"Time";
        preservedDate = getreminderTime;
    }
    else if([identifier isEqualToString:@"Pre-Alarm"])
    {
        preAlarmHours = getPreHours;
        preAlarmMins = getPreMins;
        
        if(preAlarmMins == 0 && preAlarmHours == 0)
        {
            _topLabel.text = @"No Pre-Alarm";
        }
        else if (preAlarmHours == 0 && preAlarmMins != 0)
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
        }
        else if (preAlarmMins == 0 && preAlarmHours != 0)
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs"];
        }
        else
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@, %ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
        }
        [self.datePicker setHidden:YES];
        [self.pickerView setHidden:NO];
        [self.pickerView selectRow:getPreHours inComponent:0 animated:YES];
        [self.pickerView selectRow:getPreMins inComponent:1 animated:YES];
        _BottomLabel.text = @"";
    }
    else if([identifier isEqualToString:@"Remind Me"])
    {
        _topViewHeightCons.constant = 0;
        [self.datePicker setHidden:YES];
        [self.pickerView setHidden:NO];
        [self.pickerView selectRow:[[[NSUserDefaults standardUserDefaults] valueForKey:@"Remind Me"] stringByReplacingOccurrencesOfString:@" Days Ahead" withString:@""].integerValue inComponent:0 animated:YES];
        _BottomLabel.text = @"Days ahead to remind me";
        self.title = @"Remind Me";
        preservedString = getrefillReminderDaysAlarm;
    }
    else if([identifier isEqualToString:@"Refill Time"])
    {
        _topViewHeightCons.constant = 0;
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];
        [self.datePicker addTarget:self action:@selector(datePcikerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.pickerView setHidden:YES];
        [self.datePicker setHidden:NO];
        [self.datePicker setDate:getrefillReminderTime];
        _BottomLabel.text = @"Set the time when you want to be reminded";
        self.title = @"Time";
        preservedDate = getrefillReminderTime;
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if([identifier isEqualToString:@"Pre-Alarm"])
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([identifier isEqualToString:@"Pre-Alarm"])
    {
        if(component == 0)
        {
            return 5;
        }
        else
        {
            return 60;
        }
    }
    else
    {
        return 15;
    }
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([identifier isEqualToString:@"Pre-Alarm"])
    {
        if(component == 0)
        {
            return [NSString stringWithFormat:@"%ld %@",(long)row,(long)row <= 1 ? @"Hour":@"Hours"];
        }
        else
        {
            return [NSString stringWithFormat:@"%ld %@",(long)row,(long)row <= 1 ? @"Min":@"Mins"];
        }
    }
    else
    {
        return [NSString stringWithFormat:@"%ld Days Ahead",(long)row];
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([identifier isEqualToString:@"Pre-Alarm"])
    {
        NSString * hrStr , * minStr;
        if(component == 0)
        {
            hrStr = [NSString stringWithFormat:@"%ld Hours",(long)row];
            preAlarmHours = (long)row;
        }
        else
        {
            minStr = [NSString stringWithFormat:@"%ld Mins",(long)row];
            preAlarmMins = (long)row;
        }
        setPreMins(preAlarmMins);
        setPreHours(preAlarmHours);
        if(preAlarmMins == 0 && preAlarmHours == 0)
        {
            _topLabel.text = @"No Pre-Alarm";
        }
        else if (preAlarmHours == 0 && preAlarmMins != 0)
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
        }
        else if (preAlarmMins == 0 && preAlarmHours != 0)
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs"];
        }
        else
        {
            _topLabel.text = [NSString stringWithFormat:@"%ld %@, %ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
        }
    }
    else
    {
//        NSDate *now = getreminderTime;
//        NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-row*24*60*60];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *fierDate = [formatter stringFromDate:sevenDaysAgo];
//        //NSLog(@"%@",fierDate);
        preservedString = [NSString stringWithFormat:@"%ld Days Ahead",(long)row];
    }
}
- (IBAction)datePcikerValueChanged:(id)sender
{
    NSDate * datepickerDate = self.datePicker.date;
    if([identifier isEqualToString:@"Start Date"])
    {
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                       fromDate:datepickerDate];
        [timeComponents setHour:0];
        [timeComponents setMinute:00];
        [timeComponents setSecond:0];
        
        NSDate *dtFinal = [calendar dateFromComponents:timeComponents];
        preservedDate = dtFinal;
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *fierDate = [formatter stringFromDate:dtFinal];
        //NSLog(@"%@",fierDate);
    }
    else if([identifier isEqualToString:@"Reminder Time"])
    {
        NSCalendar *calendarStartDate = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponentsStartDate = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                       fromDate:getstartDate];

        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                       fromDate:datepickerDate];
        [timeComponents setDay:timeComponentsStartDate.day];
        [timeComponents setMonth:timeComponentsStartDate.month];
        [timeComponents setYear:timeComponentsStartDate.year];
        [timeComponents setSecond:0];
        NSDate *dtFinal = [calendarStartDate dateFromComponents:timeComponents];
        preservedDate = dtFinal;
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *fierDate = [formatter stringFromDate:dtFinal];
        //NSLog(@"%@",fierDate);
    }
    else if([identifier isEqualToString:@"Refill Time"])
    {
        NSCalendar *calendarStartDate = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponentsStartDate = [calendarStartDate components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                                         fromDate:getstartDate];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *timeComponents = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond )
                                                       fromDate:datepickerDate];
        [timeComponents setDay:timeComponentsStartDate.day];
        [timeComponents setMonth:timeComponentsStartDate.month];
        [timeComponents setYear:timeComponentsStartDate.year];
        
        NSDate *dtFinal = [calendarStartDate dateFromComponents:timeComponents];
        preservedDate = dtFinal;
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *fierDate = [formatter stringFromDate:dtFinal];
        //NSLog(@"%@",fierDate);
    }
    
}
-(IBAction)donePressed:(id)sender
{
    BOOL isValueSet = NO;
    if([identifier isEqualToString:@"Start Date"])
    {
        if(preservedDate)
        {
            [[NSUserDefaults standardUserDefaults]setValue:preservedDate forKey:identifier];
            isValueSet = YES;
        }

    }
    else if([identifier isEqualToString:@"Reminder Time"])
    {
        if(preservedDate)
        {
            [[NSUserDefaults standardUserDefaults]setValue:preservedDate forKey:identifier];
            isValueSet = YES;
        }
    }
    else if([identifier isEqualToString:@"Pre-Alarm"])
    {
        isValueSet = YES;
        if(!getIsPaid)
        {
            [self performSegueWithIdentifier:@"toPaid" sender:self];
            return;
        }
    }
    else if([identifier isEqualToString:@"Remind Me"])
    {
        if(preservedString.length > 0)
        {
            [[NSUserDefaults standardUserDefaults]setValue:preservedString forKey:identifier];
            isValueSet = YES;
        }
    }
    else if([identifier isEqualToString:@"Refill Time"])
    {
        if(preservedDate)
        {
            [[NSUserDefaults standardUserDefaults]setValue:preservedDate forKey:identifier];
            isValueSet = YES;
        }
    }
    if(isValueSet)
    {
        AppDelegate * appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        [appdel updateCalendarAndNotification];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
