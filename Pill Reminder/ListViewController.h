//
//  ListViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate * appdel;
    NSMutableArray * fieldArray,*selectedEntries;
    NSString * cellID;
    NSString * selectedIndexPathRow;
    BOOL isInsertable;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString * identifier;
@property (strong, nonatomic) NSString * selecteDate;
@end
