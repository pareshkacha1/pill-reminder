//
//  PickerViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 26/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSDate * preservedDate;
    NSInteger preAlarmMins, preAlarmHours;
    NSString * preservedString;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightCons;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)datePcikerValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (strong, nonatomic) NSString * identifier;
@end
