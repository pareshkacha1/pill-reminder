//
//  MessageViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 26/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *fieldLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) NSString * identifier;
@end
