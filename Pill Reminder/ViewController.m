//
//  ViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 10/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "ViewController.h"
#import "ListViewController.h"
#import "AppDelegate.h"
#import "NoteViewController.h"
@interface ViewController ()
{
    AppDelegate * appdel;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appdel = [UIApplication sharedApplication].delegate;

    _lunarCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    _scrollDirection = FSCalendarScrollDirectionHorizontal;
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    _calendarView.appearance.titleVerticalOffset = -10;
    appdel.calendarView = _calendarView;
    [appdel PrepareReminder];
    [appdel weekStartDay];
    if(getIsPaid)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toList"])
    {
        ListViewController * listVC = (ListViewController *)segue.destinationViewController;
        listVC.identifier = @"Symptom";
        listVC.selecteDate = sender;
    }
    else if ([segue.identifier isEqualToString:@"toNotes"])
    {
        NoteViewController * noteVC = (NoteViewController *)segue.destinationViewController;
        noteVC.selecteDate = sender;
    }
}
#pragma mark - FSCalendarDataSource

- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    return nil;
}
- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date
{
    return nil;
}
- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    int eventCount = 0;
    NSMutableDictionary * notesDict = getNotes;
    NSMutableDictionary * sypmtomDict = [[NSUserDefaults standardUserDefaults] valueForKey:@"Symptom"];
    if([[notesDict allKeys] containsObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
    {
        eventCount++;
    }
    if([[sypmtomDict allKeys] containsObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
    {
        eventCount++;
    }
    return eventCount;
}
- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
    return getstartDate;
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
{
    return [calendar dateByAddingYears:1 toDate:getstartDate];
}

#pragma mark - FSCalendarDelegate
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    NSString * buttonTitle = @"Add Symptom";
    NSMutableDictionary * notesDict = [[NSUserDefaults standardUserDefaults] valueForKey:@"Symptom"];
    if([[notesDict allKeys] containsObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
    {
        buttonTitle = @"View/Add Symptom";
    }
    NSDictionary * notedict = getNotes;

    NSArray * temparray = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"Symptom"] valueForKey:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]] mutableCopy];
    NSString * combinedStuff = @"";
    NSString * tempStr = [temparray componentsJoinedByString:@","];
    if(tempStr && tempStr.length > 0)
    {
        combinedStuff = tempStr;
    }

    NSString * noteString = @"";
    tempStr = [notedict valueForKey:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]];
    if(tempStr && tempStr.length > 0)
    {
        noteString = [NSString stringWithFormat:@"NOTE : %@",[notedict valueForKey:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]]];
    }
    NSString * messageString;
    if(combinedStuff.length > 0 && noteString.length > 0)
    {
        messageString = [NSString stringWithFormat:@"SYMPTOMS : %@\n\n%@",combinedStuff,noteString];
    }
    else if(combinedStuff.length > 0)
    {
        messageString = [NSString stringWithFormat:@"SYMPTOMS : %@",combinedStuff];
    }
    else if(noteString.length > 0)
    {
        messageString = noteString;
    }
    UIAlertController * alertAction = [UIAlertController alertControllerWithTitle:nil message:messageString preferredStyle:UIAlertControllerStyleActionSheet];
    [alertAction addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self performSegueWithIdentifier:@"toList" sender:[calendar stringFromDate:date format:@"yyyy-MM-dd"]];
    }]];
    buttonTitle = @"Add Note";
    notesDict = getNotes;
    if([[notesDict allKeys] containsObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
    {
        buttonTitle = @"View/Add Note";
    }
    [alertAction addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self performSegueWithIdentifier:@"toNotes" sender:[calendar stringFromDate:date format:@"yyyy-MM-dd"]];
    }]];
    if([getcontraception isEqualToString:@"Pills"])
    {
        NSString * buttonTitle = @"Mark as taken";
        NSMutableArray * markedDates = [[[NSUserDefaults standardUserDefaults] valueForKey:@"marked"] mutableCopy];
        if([markedDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
        {
            buttonTitle = @"Mark as missed";
        }
        [alertAction addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSMutableArray * markedDates = [[[NSUserDefaults standardUserDefaults] valueForKey:@"marked"] mutableCopy];
            if(!markedDates)
            {
                markedDates = [NSMutableArray new];
            }
            if([markedDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            {
                [markedDates removeObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]];
            }
            else
            {
                [markedDates addObject:[_calendarView stringFromDate:date format:@"yyyy-MM-dd"]];
            }
            [[NSUserDefaults standardUserDefaults] setValue:markedDates forKey:@"marked"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_calendarView reloadData];
            });
        }]];
    }
    [alertAction addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
//    UIViewController *v = [[UIViewController alloc] init];
//    UILabel * lbl = [[UILabel alloc] init];
//    lbl.text = @"test";
//    lbl.frame = v.view.bounds;
//    [v.view addSubview:lbl];
//    [alertAction setValue:v forKey:@"contentViewController"];

    [self presentViewController:alertAction animated:YES completion:^{
        
    }];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
//    NSLog(@"did change to page %@",[calendar stringFromDate:calendar.currentPage format:@"MMMM yyyy"]);
}

- (void)calendarCurrentScopeWillChange:(FSCalendar *)calendar animated:(BOOL)animated
{
    _calendarHeightConstraint.constant = [calendar sizeThatFits:CGSizeZero].height;
    _tableViewTopCons.constant = [calendar sizeThatFits:CGSizeZero].height;
    [self.view layoutIfNeeded];
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    _calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    _tableViewTopCons.constant = CGRectGetHeight(bounds);
    [self.view layoutIfNeeded];
}
- (void)setSelectedDate:(NSDate *)selectedDate
{
    [_calendarView selectDate:selectedDate];
}

- (void)setFirstWeekday:(NSUInteger)firstWeekday
{
    if (_firstWeekday != firstWeekday) {
        _firstWeekday = firstWeekday;
        _calendarView.firstWeekday = firstWeekday;
    }
}
- (nullable UIImage *)calendar:(FSCalendar *)calendar imageForDate:(NSDate *)date
{
    if([getcontraception isEqualToString:@"Pills"])
    {
        NSMutableArray * markedDates = [[[NSUserDefaults standardUserDefaults] valueForKey:@"marked"] mutableCopy];

        if([appdel.startDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            if([markedDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
                return [UIImage imageNamed:@"checkMark_Red"];
            else
                return [UIImage imageNamed:@"Pill_Red"];
        else if([appdel.datesWithEventForPlacebo containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            if([markedDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
                return [UIImage imageNamed:@"checkMark_Ylw"];
            else
                return [UIImage imageNamed:@"Pill_Ylw"];
        else if([appdel.datesWithEvent containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            if([markedDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
                return [UIImage imageNamed:@"checkMark_Pink"];
            else
                return [UIImage imageNamed:@"Pill_Pink"];
        else
            return nil;
    }
    else if ([getcontraception isEqualToString:@"Ring"])
    {
        if ([date isEqualToDate:[_calendarView dateByAddingDays:21 toDate:getstartDate]])
            return [UIImage imageNamed:@"reply1"];
        else if([appdel.startDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            return [UIImage imageNamed:@"Ring_Red"];
        else if([appdel.datesWithEvent containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            return [UIImage imageNamed:@"Ring_Pink"];
        else if([appdel.datesWithEventForPlacebo containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            return nil;
        else
            return nil;
    }
    else
    {
        NSCalendar *gregorian = [NSCalendar autoupdatingCurrentCalendar];
        NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:getstartDate];
        NSInteger weekday = [comps weekday];
        
        NSDateComponents *compsForCurrentDate = [gregorian components:NSCalendarUnitWeekday fromDate:date];
        NSInteger weekdayForCurrentDate = [compsForCurrentDate weekday];
        
        if ([date isEqualToDate:[_calendarView dateByAddingDays:21 toDate:getstartDate]])
            return [UIImage imageNamed:@"reply1"];
        else if([appdel.startDates containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
        {
            if(weekday == weekdayForCurrentDate)
                return [UIImage imageNamed:@"Patch_Red"];
            else
                return [UIImage imageNamed:@"Patch_Pink"];
        }
        else if([appdel.datesWithEvent containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
        {
            if(weekday == weekdayForCurrentDate)
                return [UIImage imageNamed:@"Patch_Red"];
            else
                return [UIImage imageNamed:@"Patch_Pink"];
        }
        else if([appdel.datesWithEventForPlacebo containsObject: [_calendarView stringFromDate:date format:@"yyyy-MM-dd"]])
            return nil;
        else
            return nil;
    }
}
@end
