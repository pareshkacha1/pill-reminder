//
//  UpgradeViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 02/07/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "UpgradeViewController.h"
#define kRemoveAdsProductIdentifier @"net.Apptimist.PillReminder"//@"co.in.universe.GuitarLicks.VersionPremium"
@implementation UpgradeViewController
-(void)viewWillAppear:(BOOL)animated
{
//    [self.activityIndicator startAnimating];
//    _priceLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"Price"];
    if(getIsPaid)
    {
        [_restoreButton setEnabled:NO];
        [_upgradeButton setEnabled:NO];
    }
    [self fetchAvailableProducts];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
-(void)viewWillDisappear:(BOOL)animated
{
    productsRequest.delegate = nil;
    [productsRequest cancel];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    UILabel * textLabel = [cell viewWithTag:1];
    UILabel * subTextLabel = [cell viewWithTag:2];
    textLabel.text = @[@"Pre-Alarms",@"Auto-Snooze",@"Password Protection",@"Any Pill Layout"][indexPath.row];
    subTextLabel.text = @[@"Be notified prior to the actual time",@"You'll never miss a reminder",@"Keep your info private",@"Use any combination of active pills : 21/7,24/4,84/7 etc."][indexPath.row];
    return cell;
}
-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
             isEqualToString:kRemoveAdsProductIdentifier]) {
            SKProduct *product = [response.products objectAtIndex:0];

            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [numberFormatter setLocale:product.priceLocale];
            
            NSString *formattedString = [numberFormatter stringFromNumber:product.price];
            _priceLabel.text = formattedString;
            [[NSUserDefaults standardUserDefaults] setValue:formattedString forKey:@"Price"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"%@",formattedString);
            [self.activityIndicator stopAnimating];
        }
    } else {
//        [self.activityIndicator stopAnimating];
    }
}
- (void)purchaseSuccessful{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Your purchase successfuly done." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isPaid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
            [self purchaseSuccessful];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}
-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kRemoveAdsProductIdentifier]) {
                    [self purchaseSuccessful];
                    NSLog(@"Purchased ");
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed %@",transaction.error);
                break;
            default:
                break;
        }
    }
}

- (IBAction) restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}
-(void)fetchAvailableProducts{
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:kRemoveAdsProductIdentifier,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}
- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}
- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
    }
}
- (IBAction)tapForInAppPurchase{
    if(validProducts.count>0)
    [self purchaseMyProduct:[validProducts objectAtIndex:0]];
}
@end
