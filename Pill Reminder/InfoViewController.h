//
//  InfoViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface InfoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate,MFMailComposeViewControllerDelegate>
{
    NSArray * fieldArray;
}
@end
