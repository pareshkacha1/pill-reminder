//
//  PasswordViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 25/07/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.passcodeField setInputAccessoryView:self.toolBar];
    [self.passcodeField becomeFirstResponder];
    if(self.isRootView)
    {
        self.saveButton.hidden = YES;
    }
    else
    {
        self.forgotPasscode.hidden = YES;
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(searchStr.length < 5)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (IBAction)closeScreen:(id)sender {
    if(self.isRootView)
    {
        exit(0);
        return;
    }
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
- (IBAction)savePassCode:(id)sender {
    [self.view endEditing:YES];
    if(self.passcodeField.text.length == 4)
    {
        setpasscode(self.passcodeField.text);
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }
    else
    {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Passcode must have 4 numbers." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (IBAction)forgotPasscodePress:(id)sender {

}
- (IBAction)donePressed:(id)sender {
    [self.view endEditing:YES];
    NSString * passcode = getpasscode;
    if([passcode isEqualToString:self.passcodeField.text])
    {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }
    else
    {
        if(self.isRootView)
        {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Wrong passcode attampt." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
@end
