//
//  SettingSwitchCell.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "SettingSwitchCell.h"
#import "AppDelegate.h"
@implementation SettingSwitchCell

- (IBAction)switchPressed:(id)sender
{
    AppDelegate * appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UISwitch * selectedSwitch = (UISwitch *)sender;

    UIView *superView = selectedSwitch.superview;
    while (superView) {
        if ([superView isKindOfClass:[UITableViewCell class]])
            break;
        else
            superView = superView.superview;
    }
    SettingSwitchCell * cell = (SettingSwitchCell *) superView;
    if ([superView isKindOfClass: [SettingSwitchCell class]])
    {
        if([cell.leftLabel.text isEqualToString:@"Reminder"])
        {
            setreminder(selectedSwitch.on);
            
            [appdel updateCalendarAndNotification];
        }
        else
        {
            setrefillReminder(selectedSwitch.on);
        }
    }
}
@end
