//
//  AppDelegate.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 10/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
#import <StoreKit/StoreKit.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) NSInteger Days;
@property (nonatomic) NSInteger PlaceBo;
@property (nonatomic) BOOL isAppStarted;
@property (nonatomic) NSInteger BlankDays;
@property (strong, nonatomic) NSMutableArray *datesWithEvent;
@property (strong, nonatomic) NSMutableArray *datesWithEventForPlacebo;
@property (strong, nonatomic) NSMutableArray *startDates;
@property (weak, nonatomic) FSCalendar *calendarView;
- (void)getDaysStructure;
- (void)PrepareReminder;
- (void)weekStartDay;
-(void)pillReminderNotification;
-(void)refillReminderNotification;
-(void)updateCalendarAndNotification;
@end

