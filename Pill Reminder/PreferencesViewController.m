    //
//  PreferencesViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 02/07/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "PreferencesViewController.h"
#import "AppDelegate.h"
@implementation PreferencesViewController
-(void)viewWillAppear:(BOOL)animated
{
    if(!_isCustomSnooze)
    {
        CGRect newFrame = self.tableView.tableHeaderView.frame;
        newFrame.size.height = 0;
        self.tableView.tableHeaderView.frame = newFrame;
        days = getDaysCount;
        blanks = getBlankDays;
        restDays = !getBlankDays ? getPlaceboCount : getBlankCounts;
    }
    else
    {
        CGRect newFrame = self.tableView.tableHeaderView.frame;
        newFrame.size.height = 44;
        self.tableView.tableHeaderView.frame = newFrame;
        mins = getSnoozeMinutes;
        times = getSnoozeTimes;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!_isCustomSnooze)
        return 3;
    else if(_isCustomSnooze == 1)
        return 9;
    else
        return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_isCustomSnooze)
    {
        NSString * str = @[@"How many active pills do you take?",@"Do you take placebo (Sugar) pills?",blanks ? @"How many break days do you have?" : @"How many placebo (sugar) pills do you take?"][indexPath.row];

        if(indexPath.row != 1)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fieldCell"];
            UILabel * textLabel = [cell viewWithTag:1];
            textLabel.text = str;
            UITextField * textField = [cell viewWithTag:2];
            textField.delegate = self;
            if(indexPath.row == 0)
            {
                textField.text = [NSString stringWithFormat:@"%ld",(long)days];
            }
            else if (indexPath.row == 2)
            {
                textField.text = [NSString stringWithFormat:@"%ld",(long)restDays];
            }
            return cell;
        }
        else
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            cell.textLabel.text = str;
            cell.textLabel.font = [UIFont systemFontOfSize:14.0];
            if(!blanks)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            return cell;
        }
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        NSString * str;
        if(_isCustomSnooze == 1)
        {
            str = @[@"Every 1 min",@"Every 5 mins",@"Every 10 mins",@"Every 15 mins",@"Every 20 mins",@"Every 30 mins",@"Every 40 mins",@"Every 50 mins",@"Every 60 mins"][indexPath.row];
            cell.textLabel.text = str;
            str = [[str stringByReplacingOccurrencesOfString:@"Every " withString:@""] stringByReplacingOccurrencesOfString:@" mins" withString:@""];
            if([str integerValue] == mins)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else
        {
            str = @[@"Repeat 1 time",@"Repeat 2 times",@"Repeat 3 times",@"Repeat 4 times",@"Repeat 5 times"][indexPath.row];
            cell.textLabel.text = str;
            str = [[str stringByReplacingOccurrencesOfString:@"Repeat " withString:@""] stringByReplacingOccurrencesOfString:@" times" withString:@""];
            if([str integerValue] == times)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_isCustomSnooze)
    {
        if(indexPath.row == 1)
        {
            blanks = !blanks;
            [self.view endEditing:YES];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    else if (_isCustomSnooze == 1)
    {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        NSString * str = cell.textLabel.text;
        str = [[str stringByReplacingOccurrencesOfString:@"Every " withString:@""] stringByReplacingOccurrencesOfString:@" mins" withString:@""];
        NSInteger count = [str integerValue];
        mins = count;
    }
    else
    {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        NSString * str = cell.textLabel.text;
        str = [[str stringByReplacingOccurrencesOfString:@"Repeat " withString:@""] stringByReplacingOccurrencesOfString:@" times" withString:@""];
        NSInteger count = [str integerValue];
        times = count;
    }
    [self.tableView reloadData];
}
- (UITableViewCell *)rootView:(UIView *)mainView {
    UIView *view = mainView;
    while (![view isKindOfClass: [UITableViewCell class]]) {
        view = view.superview;
    }
    return (UITableViewCell *)view;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSIndexPath * indexPath = [self.tableView indexPathForCell:[self rootView:textField]];
    if(indexPath.row == 0)
    {
        days = [str integerValue];
    }
    else
    {
        restDays = [str integerValue];
    }
    return YES;
}
-(IBAction)donePressed:(id)sender
{
    if(!_isCustomSnooze)
    {
            setDaysCount(days);
            if(!blanks)
            {
                setPlaceboCount(restDays);
                setBlankCounts(0);
            }
            else
            {
                setBlankCounts(restDays);
                setPlaceboCount(0);
            }
            setBlankDays(blanks);
            [[NSUserDefaults standardUserDefaults]setValue:@"Custom" forKey:@"Days"];
    }
    else
    {
        if (_isCustomSnooze == 1)
        {
            setSnoozeMinutes(mins);
        }
        else
        {
            setSnoozeTimes(times);
        }
        [[NSUserDefaults standardUserDefaults] setValue:@"Custom" forKey:@"Auto-Snooze"];
    }
    AppDelegate * appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appdel updateCalendarAndNotification];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
