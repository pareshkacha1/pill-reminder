//
//  ListViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//
#import "PreferencesViewController.h"
#import "ListViewController.h"
#import <AudioToolbox/AudioToolbox.h>
@implementation ListViewController
@synthesize identifier;
-(void)viewDidLoad
{
    appdel = [UIApplication sharedApplication].delegate;
    if([identifier isEqualToString:@"Contraception"])
    {
        fieldArray = [@[@"Pills",@"Ring",@"Patch"] mutableCopy];
        cellID = @"imageCell";
    }
    else if([identifier isEqualToString:@"Days"])
    {
        fieldArray = [@[@"21/7,21 days + 7 days off",@"21+7,21 days + 7 days of placebo pills",@"24/4,24 days + 4 days off",@"24+4,24 days +4 days of placebo pills",@"28,28 days",@"35,35 days",@"84+7,84 days  + 7 days of placebo pills",@"Custom"] mutableCopy];
        cellID = @"subTitleCell";
    }
    else if([identifier isEqualToString:@"Symptom"])
    {
        fieldArray = [@[@"Custom Symptom",@"spotting",@"Light Flow",@"Heavy Flow",@"Diarhea",@"Cramps",@"intimate",@"Fatigue",@"headache",@"Moody",@"Breast Tenderness",@"Stressed",@"Acne",@"Discharge",@"Nausea",@"Vomiting"]mutableCopy];
        cellID = @"imageCell";
    }
    else if([identifier isEqualToString:@"Sound"] || [identifier isEqualToString:@"Sound "])
    {
        fieldArray = [[NSArray arrayWithArray:[self loadAudioFileList]] mutableCopy];
        cellID = @"cell";
    }
    else if([identifier isEqualToString:@"Auto-Snooze"])
    {
        fieldArray = [@[@"No Auto-Snooze",@"Repeat every minute",@"Repeat every hour",@"Custom"]mutableCopy];
        cellID = @"cell";
    }
    else if([identifier isEqualToString:@"Week Starts on"])
    {
        fieldArray = [@[@"Sunday",@"Monday",@"Saturday"]mutableCopy];
        cellID = @"cell";
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    selectedIndexPathRow = [NSString stringWithFormat:@"%ld",(unsigned long)[fieldArray indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:identifier]]];
    if([identifier isEqualToString:@"Symptom"])
    {
        selectedEntries = [[[[NSUserDefaults standardUserDefaults] valueForKey:identifier] valueForKey:_selecteDate] mutableCopy];
        if(!selectedEntries)
        {
            selectedEntries = [NSMutableArray new];
        }
    }
    if(isInsertable)
    {
        [fieldArray removeLastObject];
        [fieldArray removeLastObject];
        [fieldArray addObject:[NSString stringWithFormat:@"Every %ld %@",getSnoozeMinutes,getSnoozeMinutes <= 1 ? @"min":@"mins"]];
        [fieldArray addObject:[NSString stringWithFormat:@"Repeat %ld %@",getSnoozeTimes,getSnoozeTimes <= 1 ? @"time":@"times"]];
    }
    [self.tableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return fieldArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([identifier isEqualToString:@"Days"])
    {
        if(!getIsPaid)
        {
            if(indexPath.row != 0 && indexPath.row != 1)
            {
                cellID = @"subTitleImageCell";
            }
            else
            {
                cellID = @"subTitleCell";
            }
        }
        else
        {
            cellID = @"subTitleCell";
        }
    }
    if([identifier isEqualToString:@"Auto-Snooze"])
    {
        if(indexPath.row == 3)
            cellID = @"subTitleCell";
        else
            cellID = @"cell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if([cellID isEqualToString:@"subTitleCell"])
    {
        NSString * subStr;
        if([identifier isEqualToString:@"Days"])
        {
            NSString * str = [fieldArray objectAtIndex:indexPath.row];
            NSArray * strs = [str componentsSeparatedByString:@","];

            cell.textLabel.text = [strs firstObject];

            if(indexPath.row == 7)
            {
                if(getBlankDays)
                {
                    subStr = [NSString stringWithFormat:@"%ld %@ + %ld %@ off",getDaysCount,getDaysCount <= 1 ? @"day":@"days",getBlankCounts,getBlankCounts <= 1 ? @"day":@"days"];
                }
                else
                {
                    subStr = [NSString stringWithFormat:@"%ld %@ + %ld %@ of placebo pills",getDaysCount,getDaysCount <= 1 ? @"day":@"days",getPlaceboCount,getPlaceboCount <= 1 ? @"day":@"days"];
                }
                cell.detailTextLabel.text = subStr;
            }
            else
            {
                cell.detailTextLabel.text = [strs lastObject];
            }
        }
        else
        {
            NSString * str = [fieldArray objectAtIndex:indexPath.row];
            cell.textLabel.text = str;
            subStr = [NSString stringWithFormat:@"Every %ld %@, Repeat %ld %@",getSnoozeMinutes,getSnoozeMinutes <= 1 ? @"min":@"mins",getSnoozeTimes,getSnoozeTimes <= 1 ? @"time":@"times"];
            cell.detailTextLabel.text = subStr;
        }
    }
    else if ([cellID isEqualToString:@"subTitleImageCell"])
    {
        NSString * str = [fieldArray objectAtIndex:indexPath.row];
        NSArray * strs = [str componentsSeparatedByString:@","];
        UILabel * textLabel = [cell viewWithTag:1];
        UILabel * subTextLabel = [cell viewWithTag:2];
        textLabel.text = [strs lastObject];
        subTextLabel.text = [strs firstObject];
        NSString * subStr;

        if([identifier isEqualToString:@"Days"] && indexPath.row == 7)
        {
            if(getBlankDays)
            {
                subStr = [NSString stringWithFormat:@"%ld %@ + %ld %@ off",getDaysCount,getDaysCount <= 1 ? @"day":@"days",getBlankCounts,getBlankCounts <= 1 ? @"day":@"days"];
            }
            else
            {
                subStr = [NSString stringWithFormat:@"%ld %@ + %ld %@ of placebo pills",getDaysCount,getDaysCount <= 1 ? @"day":@"days",getPlaceboCount,getPlaceboCount <= 1 ? @"day":@"days"];
            }
            subTextLabel.text = subStr;
        }
    }
    else if ([cellID isEqualToString:@"imageCell"])
    {
        UIImageView * imgVw = [cell viewWithTag:1];
        if([[fieldArray objectAtIndex:indexPath.row] isEqualToString:@"Pills"])
        {
            imgVw.image = [UIImage imageNamed:@"Pill_Red"];
        }
        else if ([[fieldArray objectAtIndex:indexPath.row] isEqualToString:@"Ring"])
        {
            imgVw.image = [UIImage imageNamed:@"Ring_Red"];
        }
        else
        {
            imgVw.image = [UIImage imageNamed:@"Patch_Red"];
        }
        if([identifier isEqualToString:@"Days"])
        {
           imgVw.image = [UIImage imageNamed:@"lock"];
        }
        UILabel * textLbl = [cell viewWithTag:2];

        textLbl.text = [fieldArray objectAtIndex:indexPath.row];
    }
    else
    {
        cell.textLabel.text = [[fieldArray objectAtIndex:indexPath.row] lastPathComponent];
    }
    if([identifier isEqualToString:@"Symptom"])
    {
        if([selectedEntries containsObject:[fieldArray objectAtIndex:indexPath.row]])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else
    {
        if(selectedIndexPathRow.length > 0)
        {
            if(indexPath.row == selectedIndexPathRow.integerValue)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
    }
    return cell;
}
-(NSArray *)loadAudioFileList{
    NSMutableArray * tempArry = [[NSMutableArray alloc] init];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *directoryURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds"];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSLog(@"%@",url);
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [tempArry addObject:url];
        }
    }
    return tempArry;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([identifier isEqualToString:@"Contraception"])
    {
    }
    else if([identifier isEqualToString:@"Days"])
    {
        if(indexPath.row != 0 && indexPath.row != 1)
        {
            if(!getIsPaid)
            {
                [self performSegueWithIdentifier:@"toPaid" sender:self];
                return;
            }
            else
            {
                if (indexPath.row == 7)
                {
                    [self performSegueWithIdentifier:@"toPrefrence" sender:self];
                    return;
                }
            }
        }
    }
    else if([identifier isEqualToString:@"Symptom"])
    {
        if([selectedEntries containsObject:[fieldArray objectAtIndex:indexPath.row]])
        {
            [selectedEntries removeObject:[fieldArray objectAtIndex:indexPath.row]];
        }
        else
        {
            [selectedEntries addObject:[fieldArray objectAtIndex:indexPath.row]];
        }
    }
    else if([identifier isEqualToString:@"Sound"] || [identifier isEqualToString:@"Sound "])
    {
        SystemSoundID soundID;
        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)[fieldArray objectAtIndex:indexPath.row],&soundID);
        AudioServicesPlaySystemSound(soundID);
    }
    else if([identifier isEqualToString:@"Auto-Snooze"])
    {
        if(indexPath.row == 3)
        {
            if(!isInsertable)
            {
                isInsertable = YES;
                [tableView beginUpdates];
                [fieldArray addObject:[NSString stringWithFormat:@"Every %ld %@",getSnoozeMinutes,getSnoozeMinutes <= 1 ? @"min":@"mins"]];
                [fieldArray addObject:[NSString stringWithFormat:@"Repeat %ld %@",getSnoozeTimes,getSnoozeTimes <= 1 ? @"time":@"times"]];
                NSArray *paths = @[[NSIndexPath indexPathForRow:[fieldArray count]-1 inSection:0],[NSIndexPath indexPathForRow:[fieldArray count]-2 inSection:0]];
                [[self tableView] insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
                [tableView endUpdates];
            }
        }
        else if(indexPath.row < 3)
        {
            if(isInsertable)
            {
                isInsertable = NO;
                [tableView beginUpdates];
                [fieldArray removeLastObject];
                [fieldArray removeLastObject];
                NSArray *paths = @[[NSIndexPath indexPathForRow:[fieldArray count]-1 inSection:0],[NSIndexPath indexPathForRow:[fieldArray count]-2 inSection:0]];
                [[self tableView] deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationBottom];
                [tableView endUpdates];
            }
        }
        else
        {
            selectedIndexPathRow = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self performSegueWithIdentifier:@"toPrefrence" sender:self];
        }
    }
    else if([identifier isEqualToString:@"Week Starts on"])
    {
    }
    selectedIndexPathRow = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    [self.tableView reloadData];
}
-(IBAction)donePressed:(id)sender
{
    if([identifier isEqualToString:@"Symptom"])
    {
        NSMutableDictionary * notesDict = [[[NSUserDefaults standardUserDefaults] valueForKey:identifier] mutableCopy];

        if(selectedEntries.count > 0)
        {
            if(!notesDict)
            {
                notesDict = [NSMutableDictionary new];
            }
            [notesDict setValue:selectedEntries forKey:_selecteDate];
            [[NSUserDefaults standardUserDefaults] setValue:notesDict forKey:identifier];
        }
        else
        {
            [notesDict removeObjectForKey:_selecteDate];
            [[NSUserDefaults standardUserDefaults] setValue:notesDict forKey:identifier];
        }
    }
    else if([identifier isEqualToString:@"Auto-Snooze"])
    {
        if(getIsPaid)
        {
            [[NSUserDefaults standardUserDefaults]setValue:fieldArray[selectedIndexPathRow.integerValue] forKey:identifier];
            [appdel updateCalendarAndNotification];
        }
        else
        {
            [self performSegueWithIdentifier:@"toPaid" sender:self];
            return;
        }
    }
    if([identifier isEqualToString:@"Contraception"])
    {
        setcontraception(fieldArray[selectedIndexPathRow.integerValue]);
    }
    else if([identifier isEqualToString:@"Days"])
    {
        [[NSUserDefaults standardUserDefaults]setValue:fieldArray[selectedIndexPathRow.integerValue] forKey:identifier];
        [appdel updateCalendarAndNotification];

    }
    else if([identifier isEqualToString:@"Week Starts on"])
    {
        [[NSUserDefaults standardUserDefaults]setValue:fieldArray[selectedIndexPathRow.integerValue] forKey:identifier];
        [appdel weekStartDay];
    }

    if([identifier isEqualToString:@"Sound"] || [identifier isEqualToString:@"Sound "])
    {
        if(selectedIndexPathRow.integerValue < fieldArray.count)
        {
            [[NSUserDefaults standardUserDefaults]setValue:[fieldArray[selectedIndexPathRow.integerValue] lastPathComponent] forKey:identifier];
        }
    }

    [appdel updateCalendarAndNotification];

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPaid"])
    {
        
    }
    else
    {
        if([identifier isEqualToString:@"Days"])
        {
            PreferencesViewController * cv = (PreferencesViewController *)segue.destinationViewController;
            cv.isCustomSnooze = 0;
        }
        else
        {
            PreferencesViewController * cv = (PreferencesViewController *)segue.destinationViewController;
            cv.isCustomSnooze = [selectedIndexPathRow integerValue]== 4 ? 1 : 2;
        }
    }
}
@end
