//
//  InfoViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "InfoViewController.h"
@implementation InfoViewController
-(void)viewDidLoad
{
    fieldArray = @[@[@"Upgrade to Full Version",@"Restore Purchase"],@[@"Send Backup to Email"],@[@"No Sound?" , @"Recommaned to a Friend",@"Rate on App Store",@"More Apps",@"Website and Support"]];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(getIsPaid)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * arry = fieldArray[section];
    return arry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSArray * arry = fieldArray[indexPath.section];
    cell.textLabel.text = arry[indexPath.row];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return fieldArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0;
    }
    else
    {
        return 20;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    return view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            [self performSegueWithIdentifier:@"toPaid" sender:self];
        }
        else if (indexPath.row == 1)
        {
            [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        }
    }
    else if (indexPath.section == 1)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:@""];
            [self presentViewController:controller animated:YES completion:NULL];
        }
        else{
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"You cant sent mail right now." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        if(indexPath.row == 1)
        {
            NSArray *activityItems = @[[NSURL URLWithString:@"https://itunes.apple.com/us/app/birth-control-reminder-pills/id953740058?mt=8"]];
            UIActivityViewController *viewCont = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            [self presentViewController:viewCont animated:YES completion:nil];
        }
        else if (indexPath.row == 2)
        {
              [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/us/app/birth-control-reminder-pills/id953740058?mt=8"]];
        }
    }
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
