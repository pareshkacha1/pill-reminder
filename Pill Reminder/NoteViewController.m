//
//  NoteViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 29/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "NoteViewController.h"

@implementation NoteViewController
-(void)viewDidLoad
{
    NSMutableDictionary * notesDict = getNotes;
    if([[notesDict allKeys] containsObject:_selecteDate])
    {
        _textView.text = [notesDict valueForKey:_selecteDate];
    }
    [_textView setInputAccessoryView:_toolBar];
}
- (IBAction)editingFinished:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)donePressed:(id)sender {
    NSMutableDictionary * notesDict = [getNotes mutableCopy];
    if(!notesDict)
    {
        notesDict = [NSMutableDictionary new];
    }
    if(_textView.text.length == 0 && [[notesDict allKeys] containsObject:_selecteDate])
    {
        [notesDict removeObjectForKey:_selecteDate];
    }
    else
    {
        [notesDict setValue:_textView.text forKey:_selecteDate];
    }
    setNotes(notesDict);
    [self.navigationController popViewControllerAnimated:YES];
}
@end
