//
//  PasswordViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 25/07/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordViewController : UIViewController<UITextFieldDelegate>
- (IBAction)closeScreen:(id)sender;
@property (nonatomic) BOOL isRootView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *passcodeField;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasscode;
- (IBAction)forgotPasscodePress:(id)sender;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)savePassCode:(id)sender;
@end
