//
//  SettingCell.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *lockImgView;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewLeadingCons;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightCons;
@end
