//
//  PreferencesViewController.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 02/07/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferencesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSInteger days,restDays,mins,times;
    BOOL blanks;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int isCustomSnooze;
@end
