//
//  Constants.h
//  Pill Reminder
//
//  Created by Paresh Kacha on 26/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define setcontraception(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Contraception"]

#define getcontraception [[NSUserDefaults standardUserDefaults]valueForKey:@"Contraception"]

#define setreminderDays(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Days"]

#define getreminderDays [[NSUserDefaults standardUserDefaults]valueForKey:@"Days"]

#define setstartDate(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Start Date"]

#define getstartDate [[NSUserDefaults standardUserDefaults]objectForKey:@"Start Date"]

#define setreminder(value) [[NSUserDefaults standardUserDefaults]setBool:value forKey:@"Reminder"]

#define getreminder [[NSUserDefaults standardUserDefaults]boolForKey:@"Reminder"]

#define setreminderTime(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Reminder Time"]

#define getreminderTime [[NSUserDefaults standardUserDefaults]objectForKey:@"Reminder Time"]

#define setmessageForPill(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Pill)"]

#define getmessageForPill [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Pill)"]

#define setmessageForPlacebo(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Placebo)"]

#define getmessageForPlacebo [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Placebo)"]

#define setmessageForRingInsert(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Insert)"]

#define getmessageForRingInsert [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Insert)"]

#define setmessageForRingRemove(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Remove)"]

#define getmessageForRingRemove [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Remove)"]

#define setmessageForPatchApply(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Apply)"]

#define getmessageForPatchApply [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Apply)"]

#define setmessageForPatchRemove(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message (Remove) "]

#define getmessageForPatchRemove [[NSUserDefaults standardUserDefaults]valueForKey:@"Message (Remove) "]

#define setrefillReminder(value) [[NSUserDefaults standardUserDefaults]setBool:value forKey:@"Refill Reminder"]

#define getrefillReminder [[NSUserDefaults standardUserDefaults]boolForKey:@"Refill Reminder"]

#define setrefillReminderDaysAlarm(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Remind Me"]

#define getrefillReminderDaysAlarm [[NSUserDefaults standardUserDefaults]objectForKey:@"Remind Me"]

#define setrefillReminderTime(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Refill Time"]

#define getrefillReminderTime [[NSUserDefaults standardUserDefaults]objectForKey:@"Refill Time"]

#define setrefillMessage(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Message"]

#define getrefillMessage [[NSUserDefaults standardUserDefaults]valueForKey:@"Message"]

#define setWeekStart(value) [[NSUserDefaults standardUserDefaults]setValue:value forKey:@"Week Starts on"]

#define getWeekStart [[NSUserDefaults standardUserDefaults]valueForKey:@"Week Starts on"]

#define setFirstTimeLaunch(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"firstTimeLaunch"]

#define getFirstTimeLaunch [[NSUserDefaults standardUserDefaults]integerForKey:@"firstTimeLaunch"]

#define setIsPaid(value) [[NSUserDefaults standardUserDefaults]setBool:value forKey:@"isPaid"]

#define getIsPaid [[NSUserDefaults standardUserDefaults]boolForKey:@"isPaid"]

#define setNotes(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Notes"]

#define getNotes [[NSUserDefaults standardUserDefaults]objectForKey:@"Notes"]

#define setSounds(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Sound"]

#define getSounds [[NSUserDefaults standardUserDefaults]objectForKey:@"Sound"]

#define setSoundsRefill(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"Sound "]

#define getSoundsRefill [[NSUserDefaults standardUserDefaults]objectForKey:@"Sound "]

#define setDaysCount(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"daysCount"]

#define getDaysCount [[NSUserDefaults standardUserDefaults]integerForKey:@"daysCount"]

#define setPlaceboCount(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"placeboCount"]

#define getPlaceboCount [[NSUserDefaults standardUserDefaults]integerForKey:@"placeboCount"]

#define setBlankCounts(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"blankCounts"]

#define getBlankCounts [[NSUserDefaults standardUserDefaults]integerForKey:@"blankCounts"]

#define setBlankDays(value) [[NSUserDefaults standardUserDefaults] setBool:value forKey:@"setBlank"]

#define getBlankDays [[NSUserDefaults standardUserDefaults] boolForKey:@"setBlank"]

#define setPreHours(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"preHours"]

#define getPreHours [[NSUserDefaults standardUserDefaults]integerForKey:@"preHours"]

#define setPreMins(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"preMins"]

#define getPreMins [[NSUserDefaults standardUserDefaults]integerForKey:@"preMins"]

#define setSnoozeMinutes(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"snoozeMinutes"]

#define getSnoozeMinutes [[NSUserDefaults standardUserDefaults]integerForKey:@"snoozeMinutes"]

#define setSnoozeTimes(value) [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"snoozeTimes"]

#define getSnoozeTimes [[NSUserDefaults standardUserDefaults]integerForKey:@"snoozeTimes"]

#define setSnoozeNotifications(value) [[NSUserDefaults standardUserDefaults]setObject:value forKey:@"SnoozeNotifications"]

#define getSnoozeNotifications [[NSUserDefaults standardUserDefaults]objectForKey:@"SnoozeNotifications"]

#define setSnoozeClosedDate(value) [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"snoozeClosedDate"]

#define getSnoozeClosedDate [[NSUserDefaults standardUserDefaults] objectForKey:@"snoozeClosedDate"]

#define setpasscode(value) [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"passcode"]

#define getpasscode [[NSUserDefaults standardUserDefaults]valueForKey:@"passcode"]
#endif /* Constants_h */
