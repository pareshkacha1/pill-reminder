//
//  SettingViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//
#import "SettingViewController.h"
#import "ListViewController.h"
#import "PickerViewController.h"
#import "MessageViewController.h"
#import "PasswordViewController.h"
@implementation SettingViewController
-(void)viewWillAppear:(BOOL)animated
{
    NSString * messageToTake;
    NSString * messageToWait;
    if([getcontraception isEqualToString:@"Pills"])
    {
        messageToTake = @"Message (Pill)";
        messageToWait = @"Message (Placebo)";
    }
    else if([getcontraception isEqualToString:@"Ring"])
    {
        messageToTake = @"Message (Insert)";
        messageToWait = @"Message (Remove)";
    }
    else
    {
        messageToTake = @"Message (Apply)";
        messageToWait = @"Message (Remove) ";
    }
//    @"Sound",@"Sound "
    settingDict = @{@"":@[@"Contraception",@"Days",@"Start Date"],@"Reminder":@[@"Reminder",@"Reminder Time",messageToTake,messageToWait,@"Pre-Alarm",@"Auto-Snooze"],@"Refill":@[@"Refill Reminder",@"Remind Me",@"Refill Time",@"Message"],@"General":@[@"Password Lock",@"Week Starts on"]};
    [self.tableView reloadData];
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [settingDict allKeys][section];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [settingDict allKeys].count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * field = [settingDict valueForKey:[settingDict allKeys][section]];
    return field.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && (indexPath.section == 1 || indexPath.section == 2))
    {
        NSArray * field = [settingDict valueForKey:[settingDict allKeys][indexPath.section]];
        SettingSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSwitch"];
        cell.leftLabel.text = field[indexPath.row];
        if(indexPath.section == 1)
        {
            cell.rightSwitch.on = getreminder;
        }
        else
        {
            cell.rightSwitch.on = getrefillReminder;
        }
        return cell;
    }
    else
    {
        NSArray * field = [settingDict valueForKey:[settingDict allKeys][indexPath.section]];
        SettingCell *cell;
        if((indexPath.section == 1 && (indexPath.row == 4 || indexPath.row == 5)) || (indexPath.section == 3 && indexPath.row == 0))
        {
            if(!getIsPaid)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cellWithImage"];
            }
            else
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        }
        cell.leftLabel.text = field[indexPath.row];
        if(indexPath.row == 1 && indexPath.section == 0)
        {
            if([getcontraception isEqualToString:@"Pills"])
            {
                NSString * str = [[[[NSUserDefaults standardUserDefaults] valueForKey:field[indexPath.row]] componentsSeparatedByString:@","] firstObject];
                if(str.length > 0)
                {
                    cell.rightLabel.text = str;
                }
                else
                {
                    cell.rightLabel.text = @"NO";
                }
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.rightLabel.text = @"21/7";
            }
        }
        else
        {
            if(indexPath.row == 2 && indexPath.section == 0)
            {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE MMM dd, yyyy"];
                NSString *fierDate = [formatter stringFromDate:getstartDate];
                cell.rightLabel.text = fierDate;
            }
            else if(indexPath.row == 1 && indexPath.section == 1)
            {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"hh:mm a"];
                NSString *fierDate = [formatter stringFromDate:getreminderTime];
                cell.rightLabel.text = fierDate;
            }
            else if(indexPath.row == 2 && indexPath.section == 2)
            {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"hh:mm a"];
                NSString *fierDate = [formatter stringFromDate:getrefillReminderTime];
                cell.rightLabel.text = fierDate;
            }
            else if (indexPath.row == 4 && indexPath.section == 1)
            {
                if(getIsPaid)
                {
                    NSInteger preAlarmHours = getPreHours;
                    NSInteger preAlarmMins = getPreMins;
                    
                    if(preAlarmMins == 0 && preAlarmHours == 0)
                    {
                        cell.rightLabel.text = @"No Pre-Alarm";
                    }
                    else if (preAlarmHours == 0 && preAlarmMins != 0)
                    {
                        cell.rightLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
                    }
                    else if (preAlarmMins == 0 && preAlarmHours != 0)
                    {
                        cell.rightLabel.text = [NSString stringWithFormat:@"%ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs"];
                    }
                    else
                    {
                        cell.rightLabel.text = [NSString stringWithFormat:@"%ld %@, %ld %@ before",preAlarmHours,preAlarmHours <= 1 ? @"hr":@"hrs",preAlarmMins,preAlarmMins <= 1 ? @"min":@"mins"];
                    }
                }
                else
                {
                    cell.rightLabel.text = @"NO";
                }
            }
            else if (indexPath.row == 5 && indexPath.section == 1)
            {
                if(getIsPaid)
                {
                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Auto-Snooze"] isEqualToString:@"Custom"])
                    {
                        cell.rightLabel.text = [NSString stringWithFormat:@"Every %ld %@, Repeat %ld %@",getSnoozeMinutes,getSnoozeMinutes <= 1 ? @"min":@"mins",getSnoozeTimes,getSnoozeTimes <= 1 ? @"time":@"times"];
                    }
                    else
                    {
                        cell.rightLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"Auto-Snooze"];
                    }
                }
                else
                {
                    cell.rightLabel.text = @"NO";
                }
            }
            else if (indexPath.row == 0 && indexPath.section == 3)
            {
                if(getIsPaid)
                {
                    cell.rightLabel.text = @"Change Password";
                }
                else
                {
                    cell.rightLabel.text = @"NO";
                }
            }
            else
            {
                NSString * str = @"";
                if(![[NSUserDefaults standardUserDefaults] valueForKey:field[indexPath.row]])
                {
                    str = @"NO";
                }
                else
                {
                    str = [[NSUserDefaults standardUserDefaults] valueForKey:field[indexPath.row]];
                }
                cell.rightLabel.text = str;
            }
        }
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identifier = [settingDict valueForKey:[settingDict allKeys][indexPath.section]][indexPath.row];
    if(indexPath.section == 0)
    {
       if(indexPath.row == 0)
           [self performSegueWithIdentifier:@"toList" sender:identifier];
       else if(indexPath.row == 1)
       {
           if([getcontraception isEqualToString:@"Pills"])
           {
               [self performSegueWithIdentifier:@"toList" sender:identifier];
           }
       }
       else if(indexPath.row == 2)
           [self performSegueWithIdentifier:@"toPicker" sender:identifier];
       else if(indexPath.row == 3)
           [self performSegueWithIdentifier:@"toList" sender:identifier];
    }
    else if(indexPath.section == 1)
    {
        if(indexPath.row == 1)
            [self performSegueWithIdentifier:@"toPicker" sender:identifier];
//        else if(indexPath.row == 2)
//            [self performSegueWithIdentifier:@"toList" sender:identifier];
        else if(indexPath.row == 2)
            [self performSegueWithIdentifier:@"toMessage" sender:identifier];
        else if(indexPath.row == 3)
            [self performSegueWithIdentifier:@"toMessage" sender:identifier];
        else if(indexPath.row == 4)
            [self performSegueWithIdentifier:@"toPicker" sender:identifier];
        else if(indexPath.row == 5)
            [self performSegueWithIdentifier:@"toList" sender:identifier];
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == 1)
            [self performSegueWithIdentifier:@"toPicker" sender:identifier];
        else if(indexPath.row == 2)
            [self performSegueWithIdentifier:@"toPicker" sender:identifier];
//        else if(indexPath.row == 3)
//            [self performSegueWithIdentifier:@"toList" sender:identifier];
        else if(indexPath.row == 3)
            [self performSegueWithIdentifier:@"toMessage" sender:identifier];
    }
    else if(indexPath.section == 3)
    {
        if(indexPath.row == 1)
            [self performSegueWithIdentifier:@"toList" sender:identifier];
        else
        {
            if(getIsPaid)
            {
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle:nil];
                PasswordViewController * passwordScreen =
                [storyboard instantiateViewControllerWithIdentifier:@"PasswordViewController"];
                passwordScreen.isRootView = NO;
                [self presentViewController:passwordScreen
                                   animated:YES
                                 completion:nil];
            }
            else
            {
                [self performSegueWithIdentifier:@"toPaid" sender:identifier];
            }
        }
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toList"])
    {
        ListViewController * listVC = (ListViewController *)segue.destinationViewController;
        listVC.identifier = (NSString *)sender;
    }
    else if([segue.identifier isEqualToString:@"toPicker"])
    {
        PickerViewController * pickerVC = (PickerViewController *)segue.destinationViewController;
        pickerVC.identifier = (NSString *)sender;
    }
    else if([segue.identifier isEqualToString:@"toPaid"])
    {
    }
    else
    {
        MessageViewController * messageVC = (MessageViewController *)segue.destinationViewController;
        messageVC.identifier = (NSString *)sender;
    }
}
@end
