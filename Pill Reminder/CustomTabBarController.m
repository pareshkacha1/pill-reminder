//
//  CustomTabBarController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 23/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "CustomTabBarController.h"
#import "AppDelegate.h"
#import "PasswordViewController.h"
@implementation CustomTabBarController
- (void)viewDidLoad
{
    [super viewDidLoad];
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        for (UITabBarItem *tbi in self.tabBar.items) {
//            if([self.tabBar.items indexOfObject:tbi] == 1)
//            {
//                tbi.selectedImage = [[UIImage imageNamed:@"info"]
//                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//                
//                self.tabBarItem.image = [[UIImage imageNamed:@"info"]
//                                         imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//            }
//            else
//            {
//                tbi.selectedImage = [[UIImage imageNamed:@"calendar"]
//                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//                
//                self.tabBarItem.image = [[UIImage imageNamed:@"calendar"]
//                                         imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//            }
//        }
//    }
}
-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate * appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if(appdel.isAppStarted)
    {
        NSString * passcode = getpasscode;
        if(passcode.length > 0)
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle:nil];
            PasswordViewController * passwordScreen =
            [storyboard instantiateViewControllerWithIdentifier:@"PasswordViewController"];
            passwordScreen.isRootView = YES;
            [self presentViewController:passwordScreen
                               animated:YES
                             completion:^{
                                 appdel.isAppStarted = NO;
                             }];
        }
    }
}
@end
