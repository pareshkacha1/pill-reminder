//
//  MessageViewController.m
//  Pill Reminder
//
//  Created by Paresh Kacha on 26/06/16.
//  Copyright © 2016 Paresh Kacha. All rights reserved.
//

#import "MessageViewController.h"

@implementation MessageViewController
@synthesize identifier,textField;
-(void)viewDidLoad
{
    if([identifier isEqualToString:@"Message (Apply)"])
    {
        textField.placeholder = @"Apply new patch";
    }
    else if([identifier isEqualToString:@"Message (Pill)"])
    {
        textField.placeholder = @"Take a pill";
    }
    else if([identifier isEqualToString:@"Message (Placebo)"])
    {
        textField.placeholder = @"Take a placebo pill";
    }
    else if([identifier isEqualToString:@"Message (Insert)"])
    {
        textField.placeholder = @"Insert new ring";
    }
    else if([identifier isEqualToString:@"Message (Remove)"])
    {
        textField.placeholder = @"Remove ring";
    }
    else if([identifier isEqualToString:@"Message (Remove) "])
    {
        textField.placeholder = @"Remove patch";
    }
    else if([identifier isEqualToString:@"Message"])
    {
        textField.placeholder = @"Get Contraception";
    }
    NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:identifier];
    if(![str isEqualToString:textField.placeholder])
    {
        textField.text = str;
    }
    self.title = @"Message";
}
-(IBAction)donePressed:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setValue:textField.text.length>0?textField.text:textField.placeholder forKey:identifier];
    AppDelegate * appdel = [UIApplication sharedApplication].delegate;
    
    [appdel updateCalendarAndNotification];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
